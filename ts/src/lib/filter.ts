import { MapOfFiltered } from './mapOfFiltered';

import { WordsDictionary } from './wordsDictionary';


export class Filter {

    private initialMaps: object[];

    private cacheOfFilteredCollection: object[];

    private dictionaryOfResearchedTerms: any;

    constructor() {
        this.initialMaps = [];
        this.cacheOfFilteredCollection = [];
        this.dictionaryOfResearchedTerms = {};
    }

    getMaps(
        collection: object[],
        term: string = '',
        ...properties: (string | number | symbol)[]): object[] {

        let filtered: any[] = [];

        let map: MapOfFiltered | null;

        let maps: object[] = [];

        if (this.dictionaryOfResearchedTerms.hasOwnProperty(term)) {
            collection = this.dictionaryOfResearchedTerms[term];
        } else if (this.cacheOfFilteredCollection.length) {
            collection = this.cacheOfFilteredCollection;
        }

        if (!properties.length) {
            // Reflect.ownKeys pega propriedades não-enumeráveis
            properties = Object.keys(collection[0]);
        }

        if (term === '') {
            if (!this.dictionaryOfResearchedTerms.hasOwnProperty(term)) {
                collection.forEach(
                    (item: object) => {
                        map = {
                            source: item
                        };
                        this.initialMaps.push(map);
                    }
                );
            }

            filtered = collection;
            maps = this.initialMaps;
        } else {
            collection.forEach(
                (item: any) => {
                    for (const property of properties) {
                        map = this.mapIfFound(term, item[property]);
                        if (map) {
                            filtered.push(item);
                            map.source = item;
                            maps.push(map);
                            break;
                        }
                    }
                }
            );
        }

        this.cacheOfFilteredCollection = filtered;
        this.dictionaryOfResearchedTerms[term] = filtered;

        return maps;
    }

    private mapIfFound(term: string = '', text: string = ''): MapOfFiltered | null {

        let termIndex: number;

        let amountFound = 0;

        let foundWordIndex = -1;

        let pieceOfLastTermFound = ``;

        let lastWordFound = ``;

        let indexOfLastFoundWord = -1;

        let lastTermFoundWasWhole = false;

        const piecesOfTerms = term.trim().split(' ').filter(item => item !== '');

        const words = text.trim().split(' ');

        const wordsDictionary: WordsDictionary = this.getAsCountable(words);

        const map: MapOfFiltered = {
            terms: piecesOfTerms,
            mapping: {}
        };

        if (wordsDictionary.length! >= piecesOfTerms.length) {

            for (const termPiece of piecesOfTerms) {

                if (amountFound) {
                    foundWordIndex = this.indexOfTerm(wordsDictionary, termPiece, true);
                } else {
                    foundWordIndex = this.indexOfTerm(wordsDictionary, termPiece);
                }

/*
 * A partir do primeiro termo a busca retorna algo se o termo coincidir com qualquer parte do texto; quando houver dois ou mais termos para coincidirem, a busca só retorna algo em dois casos a saber: se o termo procurado estiver à extrema esquerda dos termos procurados e for apenas parte de qualquer palavra do texto, o termo imediatamente à direita deste último deve coincidir com o início da palavra imediatamente à direita da última palavra do texto encontrada; se último termo encontrado coincidiu com uma palavra inteira do texto, o termo à extrema direita dos termos procurados poderá coincidir, ou com qualquer palavra à direita da última palavra encontrada, mesmo que aquela não seja vizinha desta, ou com o início daquela, caso o termo seja apenas parte daquela.
 */
                if (foundWordIndex !== -1
                    && (
                        lastTermFoundWasWhole
                        || !amountFound
                        || (
                            foundWordIndex === indexOfLastFoundWord + 1
                            // o `i` força ignorar case sensitive
                            && lastWordFound.search(new RegExp(`${pieceOfLastTermFound}$`, `i`))
                                !== -1
                            && amountFound === 1
                        )
                    )
                ) {
                    pieceOfLastTermFound = termPiece;
                    lastWordFound = words[foundWordIndex];
                    termIndex = lastWordFound.search(new RegExp(termPiece, 'i'));

                    (map.mapping as any)[foundWordIndex] = {
                        researchedPiece: termPiece,
                        termIndex: termIndex
                    };

                    amountFound += 1;

                    if (termPiece.toUpperCase()
                        === (wordsDictionary as any)[foundWordIndex].toUpperCase()
                        )
                    {
                        lastTermFoundWasWhole = true;
                    } else {
                        lastTermFoundWasWhole = false;
                    }

                    indexOfLastFoundWord = foundWordIndex;

                    // to search only in order
                    do {
                        delete (wordsDictionary as any)[foundWordIndex];
                    }
                    while (--foundWordIndex >= 0);
                } else {
                    break;
                }
            }

            if (amountFound === piecesOfTerms.length) {
                return map;
            }
        }

        return null;
    }

    private indexOfTerm(
        wordsMap: any,
        term: string,
        searchFromBeginning: boolean = false): number {

        let regexp: RegExp;

        let word: string;

        term = term.trim();

        if (searchFromBeginning) {
            term = `^${term}`;
        }

        regexp = new RegExp(term, `i`);

        for (const wordIndex in wordsMap) {
            word = wordsMap[wordIndex];
            if (word.search(regexp) !== -1) {
                return parseInt(wordIndex);
            }
        }

        return -1;
    }

    private getAsCountable(collection: object): any {

        const getAccessors: (object: object) => object = (object: object) => {
            const calculateLengthOf: (...args: any[]) => number = (object: object) => {
                return Object.keys(object).length;
            };

            const length: number = calculateLengthOf(object);

            const getLengthAccessors: (...args: any[]) => object = (length: number) => Object
                .defineProperties({}, {
                    get: {
                        value: () => {
                            return length;
                        }
                    },
                    set: {
                        value: () => {
                            length = calculateLengthOf(object);
                        }
                    }
                }
                );

            return Object.defineProperties(object, {
                length: getLengthAccessors(length)
            });
        };

        return getAccessors(Object.assign({}, collection));
    }

}
