export type MapOfFiltered = {

    source?: object;

    terms?: string[];

    mapping?: {
        researchedPiece?: string;
        termIndex?: number
    }

}
